package com.ks.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * 角色
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/27
 */
@Entity
//@PrimaryKeyJoinColumn(name = "id")
public class SysRole {
    public SysRole() {
        super();
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", length = 40)
    @NotNull(message="不能为空")
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
