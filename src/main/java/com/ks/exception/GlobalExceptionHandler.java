package com.ks.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.RespectBinding;
import java.util.List;
import com.ks.utils.JSONError;

/**
 * 全局异常处理
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/24
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public String ValidationFailedErrorHandler(HttpServletRequest req, MethodArgumentNotValidException ex) throws Exception{
//        ErrorInfo<String> r = new ErrorInfo<>();
        StringBuilder sb = new StringBuilder();
        List<FieldError> fieldErrors=ex.getBindingResult().getFieldErrors();

        for (FieldError error:fieldErrors) {
            sb.append(error.getField() + "：" + error.getDefaultMessage() + "，");
        }

        sb.deleteCharAt(sb.length() - 1);

        return JSONError.fillJSONError(HttpStatus.UNPROCESSABLE_ENTITY, sb.toString(), req.getRequestURI(), req.getQueryString());

    }

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String notFoundErrorHandler(HttpServletRequest req, NotFoundException e) throws Exception {

        return JSONError.fillJSONError(HttpStatus.NOT_FOUND, e.getMessage(), req.getRequestURI(), req.getQueryString());

    }

    @ExceptionHandler(value = AuthenticationException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public String authenticationErrorHandler(HttpServletRequest req, AuthenticationException e) throws Exception {
        String message = e.getMessage();
        if(e instanceof BadCredentialsException){
            message = "密码错误";
        }

        if(e instanceof AccountExpiredException){
            message = "账户过期";
        }

        if(e instanceof LockedException){
            message = "账户锁定";
        }

        if(e instanceof DisabledException){
            message = "账户不可用";
        }

        if(e instanceof CredentialsExpiredException){
            message = "证书过期";
        }

        return JSONError.fillJSONError(HttpStatus.UNAUTHORIZED, message, req.getRequestURI(), req.getQueryString());
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public String AccessDeniedHandler(HttpServletRequest req, AccessDeniedException e) throws Exception {

        return JSONError.fillJSONError(HttpStatus.FORBIDDEN, e.getMessage(), req.getRequestURI(), req.getQueryString());

    }
}
