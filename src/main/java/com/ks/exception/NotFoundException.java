package com.ks.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 自定义异常，没有找到资源
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/24
 */
public class NotFoundException extends BaseException {
    public NotFoundException() {
        super("没有找到资源！");
    }

    public NotFoundException(String message){
        super(message);
    }
}
