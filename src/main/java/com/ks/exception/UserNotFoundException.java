package com.ks.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @author kushao <kushaobuy@163.com>
 * @version 2017-05-19
 */
public class UserNotFoundException extends AuthenticationException {

    private static final long serialVersionUID = 1L;

    public UserNotFoundException(String msg) {
        super(msg);
    }

    public UserNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }
}
