package com.ks.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import com.ks.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    List<City> findAll();
    City findOne(Long id);
    long count();
    City save(City city);
    void delete(Long id);

    Page<City> findAll(Pageable pageable);

    List<City> findByNameLike(String name);

    Page<City> findByNameLike(Pageable pageable, String name);
}
