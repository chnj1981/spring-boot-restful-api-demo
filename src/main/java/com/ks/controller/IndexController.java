package com.ks.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * kuShao 2017/4/22.
 */
@RestController
@RequestMapping("/")
@Api(tags = "Index", value = "Index", description = "描述与版本")
public class IndexController {

    @RequestMapping(value = "/echo", method = RequestMethod.GET)
    @ApiOperation(value = "服务描述与版本", notes = "服务描述与版本", code = 200, produces = "text/html")
    public String echo(){
        return "spring boot RESTFul API demo \r\n" +
                "version 1.0.0";
    }


}
