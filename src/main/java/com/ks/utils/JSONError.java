package com.ks.utils;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;

/**
 * 错误消息
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/28
 */
public class JSONError {

    public static String fillJSONError(HttpStatus status, String message, String url, String query){
        JSONObject jsonObject = new JSONObject(){{
            put("status", status.value());
            put("message", message);
            put("url", url);
            put("query", query);
        }};

        return jsonObject.toString();
    }
}
