package com.ks.controller;

import com.ks.entity.City;
import com.ks.entity.SysUser;
import com.ks.utils.Log;
import com.ks.utils.Result;
import com.ks.exception.NotFoundException;
import com.ks.repository.CityRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * kuShao 2017/4/24.
 */
@RestController
@RequestMapping("/cities")
@Api(tags = "City", value = "City", description = "城市")
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @ApiOperation(value="分页返回城市列表")
    @RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNumber", value = "第几页", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "页大小", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "name", value = "搜索关键字", paramType = "query", dataType = "String")
    })
    @PreAuthorize("hasRole('ROLE_USER')")
    public Page<City> getPageCities(
            @RequestParam(value = "pageNumber") int pageNumber,
            @RequestParam(value = "pageSize") int pageSize,
            @RequestParam(value = "name", required = false) String name){

        SysUser userDetails = (SysUser) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        Log.info("当前登录用户：" + userDetails.getUsername());

        PageRequest pageRequest = new PageRequest(pageNumber - 1, pageSize);

        if(name.isEmpty() && name == null){
            name = null;
        }else{
            name = "%" + name + "%";
        }

        return cityRepository.findByNameLike(pageRequest, name);
    }

    @ApiOperation(value = "根据id返回城市")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "城市id", required = true, paramType = "path", dataType = "Long")
    })
    public City getCity(@PathVariable("id") Long id) throws NotFoundException{
        City city = cityRepository.findOne(id);

        if(city == null){
            throw new NotFoundException("没找到相关城市！");
        }

        return city;
    }

    @ApiOperation(value = "新增城市")
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "city", value = "城市", required = true, paramType = "body", dataType = "City")
    })
    public Result addCity(@RequestBody @Valid City city){
        cityRepository.save(city);

        return Result.fillResult(true, "成功", city);
    }

    @ApiOperation(value = "更新城市")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "城市ID", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "city", value = "城市", required = true, paramType = "body", dataType = "City")
    })
    public Result updateCity(@PathVariable Long id, @RequestBody @Valid City city){
        city.setId(id);
        cityRepository.save(city);

        return Result.fillResult(true, "成功", city);
    }

    @ApiOperation(value = "删除城市")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "城市ID", required = true, paramType = "path", dataType = "Long")
    })
    public Result deleteCity(@PathVariable Long id){
        cityRepository.delete(id);

        return Result.fillResult(true, "成功", null);
    }

}
