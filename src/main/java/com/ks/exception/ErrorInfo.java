package com.ks.exception;

/**
 *
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/24
 */
public class ErrorInfo<T> {

    public static final Integer OK = 0;
    public static final Integer ERROR = 400;
    public static final Integer NOT_FOUND = 404;
    public static final Integer UNPROCESSABLE_ENTITY = 422;

    private Integer code;
    private String message;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static Integer getOK() {
        return OK;
    }

    public static Integer getERROR() {
        return ERROR;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
