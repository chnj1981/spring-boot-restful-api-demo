package com.ks.repository;

import com.ks.entity.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/27
 */
@Repository
public interface SysRoleRepository extends JpaRepository<SysRole, Long> {

    SysRole save(SysRole sysRole);

    SysRole findOne(Long id);

}
