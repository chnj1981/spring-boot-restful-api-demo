package com.ks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ks.entity.SysUser;
import org.springframework.stereotype.Repository;

/**
 * @author kuShao <kushaobuy@163.com>
 * @version 2017/4/27
 */
@Repository
public interface SysUserRepository extends JpaRepository<SysUser, Long> {
    SysUser findByUsername(String username);

    SysUser save(SysUser sysUser);
}
